class Progressbar{
	
    // Aerin Sistemas Dev 
    constructor() {
        // Tambien se podria poner como otra clase
        // y que Progressbar heredase de makeAjases
        this.ajases = new makeAjases();
        this.connectSockets();

    }


    static update_progressbar(data) {
        $.each($(".progress-bar[pk="+data.payload.pk+"]"), function(){
            this.style["width"] = data.payload.data.quantity+"%";
            this.innerHTML = data.payload.data.quantity+"%"
        })
    }

    static delete_progressbar(data) {
        $.each($(".progress-bar[pk="+data.payload.pk+"]"), function(){
            this.parentElement.remove();
        })
    }

    static create_progressbar(data) {
        // Creo contenedor para la progressbar
        let new_content_progress_bar = document.createElement("div");
        // Creo texto para la progressbar 
        new_content_progress_bar.classList.add("progress");
        new_content_progress_bar.classList.add("progress");
        new_content_progress_bar.setAttribute("pk", data.payload.pk);
        new_content_progress_bar.style = "height: 100px; padding : 1em; margin : 3em;";
        let new_progress_bar = document.createElement("div");
        let new_content = document.createTextNode(data.payload.data.quantity+"%");
        new_progress_bar.classList.add("progress-bar");
        new_progress_bar.classList.add("progress-bar-striped");
        new_progress_bar.setAttribute("pk", data.payload.pk);
        new_progress_bar.setAttribute("role", "progressbar");
        new_progress_bar.setAttribute("aria-valuenow", data.payload.data.quantity);
        new_progress_bar.setAttribute("aria-valuemin", data.payload.data.minim);
        new_progress_bar.setAttribute("aria-valuemax", data.payload.data.maxim);
        new_progress_bar.style = "width: "+data.payload.data.quantity+"%;";
        let content = document.getElementById("content");
        new_content_progress_bar.appendChild(new_progress_bar);
        content.appendChild(new_content_progress_bar);
    }

    static manage_actions(message) {

        var data = JSON.parse(message.data);
            
        if (data.stream == "progressbar"){
            if (data.payload.action == "update")
                Progressbar.update_progressbar(data);
            else if (data.payload.action == "create")
                Progressbar.create_progressbar(data);
            else if (data.payload.action == "delete")
                Progressbar.delete_progressbar(data)
        }
    }


    connectSockets()  {
        console.log("Updating websockets through "+this.ws_scheme + '://' + window.location.host + "/demo/binding/")
        this.ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
        this.ws_path = this.ws_scheme + '://' + window.location.host + "/demo/binding/";
        var custom_location = "";
        this.socket = new ReconnectingWebSocket(this.ws_path);
        this.socket.onmessage = function(message){
            Progressbar.manage_actions(message);
        };
        // Helpful debugging
        this.socket.onopen = () => { console.log("Connected to notification socket"); }
        this.socket.onclose = () => { console.log("Disconnected to notification socket"); }
        // var ws_path = ws_scheme + '://' + window.location.host + window.location.pathname + "binding/"; 
    }
}