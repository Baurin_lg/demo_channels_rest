// Ajax Requests
function makeAjases() {
    // Aerin Sistemas Dev    
    query = "";

    this.makeajax = function(arg,callback,callbackerror){
        queryajax(arg, callback, callbackerror);
    }

    
    var queryajax = function(arg,callback,callbackerror){
        
        ajaxCSRFTokenSetup();        
        $.ajax({
          url:arg.url,
          type: arg.method,
          dataType: "json",
          data: { 
                'csrfmiddlewaretoken': getCookie("csrftoken"),
                'data':arg.data 
          }, 
          success: function(data){
            if (callback != undefined) callback(data);
          },
          error: function(qXHR, textStatus, errorThrown){

          	if (callbackerror != undefined) callbackerror(qXHR, textStatus, errorThrown);
            // console.log(textStatus)
          },
        });
    }

    

    var ajaxCSRFTokenSetup = function(){
        var csrftoken = getCookie("csrftoken");
        function csrfSafeMethod(method) {
		    // these HTTP methods do not require CSRF protection
		    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
		}
		$.ajaxSetup({
		    beforeSend: function(xhr, settings) {
		        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
		            xhr.setRequestHeader("X-CSRFToken", csrftoken);
		        }
		    }
		});
    }


	var getCookie = function(name) {
	    var cookieValue = null;
	    if (document.cookie && document.cookie != '') {
	        var cookies = document.cookie.split(';');
	        for (var i = 0; i < cookies.length; i++) {
	            var cookie = jQuery.trim(cookies[i]);
	            if (cookie.substring(0, name.length + 1) == (name + '=')) {
	                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                break;
	            }
	        }
	    }
	    return cookieValue;
	}

}