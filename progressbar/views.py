# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .models import *

# Create your views here.


def home(request):

	"""
	Demo View for Progress bar app
	"""

	dictionary = {
		"progressbars" : ProgressBar.objects.all(), 
	}

	return render(request, "main_progressbar.html", dictionary) 