from channels.generic.websockets import WebsocketDemultiplexer
# from dashboard.models import *
from progressbar.models import *


class Demultiplexer_progressbar(WebsocketDemultiplexer):

    consumers = {
        
        "progressbar": ProgressBarBinding.consumer,

    }

    groups = ["binding.values"]
