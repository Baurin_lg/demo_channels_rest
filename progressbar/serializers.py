from rest_framework import serializers
from .models import ProgressBar

from rest_framework import viewsets
from rest_framework import serializers
from django.contrib.auth.models import User


class ProgressBarSerializer(serializers.ModelSerializer):

    class Meta:

        model = ProgressBar
        fields = ('id', 'quantity', 'minim', 'maxim',)


class ProgressBarViewSet(viewsets.ModelViewSet):
    
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = ProgressBar.objects.all()
    serializer_class = ProgressBarSerializer
 


# Pongo aqui el serializador y la vista para el 
# recurso User pero se podria poner en otro sitio 

class UserSerializer(serializers.ModelSerializer):

    class Meta:

        model = User
        fields = ('username', 'email')


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer