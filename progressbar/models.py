# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from channels.binding.websockets import WebsocketBinding

# Create your models here.


class ProgressBar(models.Model):

	"""
	Demo Model for progress bar view
	"""

	quantity = models.FloatField(default = 0)
	minim    = models.FloatField(default = 0)
	maxim    = models.FloatField(default = 100)


class ProgressBarBinding(WebsocketBinding):

    model = ProgressBar
    stream = "progressbar"
    fields = ["__all__"]


    @classmethod
    def group_names(cls, *args, **kwargs):
        return ["binding.values"]

    def has_permission(self, user, action, pk):
        return True