from rest_framework import routers
from progressbar.serializers import *


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'progressbar', ProgressBarViewSet)