from channels import route_class, route
from progressbar.consumer import Demultiplexer_progressbar


channel_routing = [
    route_class(Demultiplexer_progressbar, path="^/demo/binding/"),
    # route_class(Demultiplexer_smartCS, path="^/smartCS/binding/"),
]